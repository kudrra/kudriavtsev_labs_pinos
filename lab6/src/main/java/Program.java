public class Program {
    public static void main(String[] args) throws Exception {
        System.out.println("Choose operation:");
        System.out.println("1. Main Process");
        System.out.println("2. Worker");

        java.util.Scanner scanner = new java.util.Scanner(System.in);
        String choice = scanner.nextLine();
        if (choice.equals("1")) {
            MainProcess.run();
        } else if (choice.equals("2")) {
            Worker.run();
        } else {
            System.out.println("Invalid choice.");
        }
        scanner.close();
    }
}