import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class ArrCount {

    public static int size = 10000;

    public static List<Integer> parstreamCalc(List<Integer> arr1, List<Integer> arr2,  int size, int sleep){
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            res.add(i);
        }
        res.parallelStream().forEach((index) -> {
            res.set(index, arr1.get(index) * arr2.get(index));
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        return res;
    }

    public static List<Integer> syncCalc(List<Integer> arr1, List<Integer> arr2,  int size, int sleep){
        List<Integer> res = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            res.add(arr1.get(i) * arr2.get(i));
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return res;
    }

    public static void main(String[] args) {

        int min = 0, max = 100, sleep = 0;
        List<Integer> arrlist1 = new ArrayList<>();
        List<Integer> arrlist2 = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        Random rand = new Random();

        for (int i = 0; i < size; i++) {
            arrlist1.add(i, rand.nextInt(min, max));
            arrlist2.add(i, rand.nextInt(min, max));
        }

        //sleep = 0

        long time1 = System.currentTimeMillis();
        result =  syncCalc(arrlist1, arrlist2, size, sleep);
        System.out.printf("default calc(sleep = 0) : %s ms\n", System.currentTimeMillis() - time1);

        time1 = System.currentTimeMillis();
        result =  parstreamCalc(arrlist1, arrlist2, size, sleep);
        System.out.printf("parallel calc(sleep = 0) : %s ms\n", System.currentTimeMillis() - time1);

        //sleep = 1
        sleep = 1;
        time1 = System.currentTimeMillis();
        result =  syncCalc(arrlist1, arrlist2, size, sleep);
        System.out.printf("default calc(sleep = 1) : %s ms\n", System.currentTimeMillis() - time1);

        time1 = System.currentTimeMillis();
        result =  parstreamCalc(arrlist1, arrlist2, size, sleep);
        System.out.printf("parallel calc(sleep = 1) : %s ms\n", System.currentTimeMillis() - time1);

    }

}
