import java.util.Scanner;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class FibAsync {

    private static int fibNum(int n)    {
        if(n<=1)
        {
            return n;
        }
        else
        {
            return fibNum(n-1)+fibNum(n-2);
        }
    }

    private static final Logger logf = Logger.getLogger(FibAsync.class.getName());

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Enter the number of the fibonacci number... ");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        CompletableFuture<Integer> potok = CompletableFuture.supplyAsync(() -> fibNum(n));
        while(!potok.isDone())
        {
            logf.info("Waiting...");
            Thread.sleep(200);
        }
        int res = potok.get();
        System.out.println("The " +n +" fibonacci number is " +res);
    }



}
