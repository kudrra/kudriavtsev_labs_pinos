import java.util.Scanner;
import java.io.File;

public class Main {
    public static void main(String[] args) {
        String currentDir = System.getProperty("user.dir");
        System.out.println("Folder tree for directory: " + currentDir);
        printFolderTree(new File(currentDir), 0);
    }

    public static void printFolderTree(File folder, int level) {
        if (!folder.isDirectory()) {
            return;
        }

        String prefix = "";
        for (int i = 0; i < level; i++) {
            prefix += "  ";
        }

        System.out.println(prefix + "+-- " + folder.getName());
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                printFolderTree(file, level + 1);
            } else {
                System.out.println(prefix + "    |-- " + file.getName());
            }
        }
    }
}
