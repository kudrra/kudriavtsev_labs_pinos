import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;

public class ThreadWrite {
    private static volatile int counter = 0;
    private static volatile int lastcount = 0;

    public static void main(String[] args) {
        Thread thread1 = new Thread(new CounterThread("Thread 1", 250));
        Thread thread2 = new Thread(new CounterThread("Thread 2", 500));
        Thread thread3 = new Thread(new CounterThread("Thread 3", 1000));

        thread1.start();
        thread2.start();
        thread3.start();
    }

    private static class CounterThread implements Runnable {
        private String threadName;
        private int interval;

        public CounterThread(String threadName, int interval) {
            this.threadName = threadName;
            this.interval = interval;
        }
        public synchronized void run() {
            {
                while (counter < 240) {
                    try (PrintWriter writer = new PrintWriter(new FileWriter("output.txt", true)))
                    {
                        LocalTime currentTime = LocalTime.now();
                        if(counter!=lastcount)
                        {
                            writer.println(threadName + " - " + currentTime + " - Counter: " + counter);
                            lastcount = counter;
                        }
                        if(threadName=="Thread 1")
                        {
                            counter++;
                        }

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}